/**
 * 
 */
package org.mycompany;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
//import org.postgresql.ds.PGSimpleDataSource;

/**
 * @author arza
 *
 */
public class CamelRoute implements Processor{
	Connection conn;
	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		connection(exchange);
		
	}
	
	private void connection(Exchange exchange) {

		try {
//			PGSimpleDataSource source = new PGSimpleDataSource();

//			conn = DriverManager.getConnection("jdbc:mysql://sampledb:3306/sampledb", "admindb", "passworddb");

			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/sampledb", "pgadmin", "P@ssw0rd");
			
//			conn = DriverManager.getConnection("jdbc:postgresql://postgresql:5432/sampledb", "admindb", "passworddb");
			
//			PGSimpleDataSource source = new PGSimpleDataSource();
//			source.setServerName("172.30.161.142");
//			source.setPortNumber(5432);
//			source.setDatabaseName("sampledb");
//			source.setUser("admindb");
//			source.setPassword("passworddb");
//			conn = source.getConnection();
			exchange.getOut().setBody("Connected");
		} catch (Exception e) {
			e.printStackTrace();
			exchange.getOut().setBody("Failed");
		}

	}

}
